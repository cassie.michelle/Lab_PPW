from django.shortcuts import render
from datetime import datetime, date

# Enter your name here
mhs_name = 'Cassie Michelle' # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(1999,8,16 ) #TODO Implement this, format (Year, Month, Date)
npm = 1706043506 # TODO Implement this
hobby = 'sleeping, watching TV, playing games, swimming'
des = 'an ordinary person with unordinary attitude...'
kelas = 'PPW-E Fasilkom UI'


# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year), 'npm': npm, 'hobby': hobby, 'description': des, 'kelas':kelas}
    return render(request, 'index_lab1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0


